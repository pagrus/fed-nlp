# fed-nlp

This is meant to be an easy intro to some NLP techniques that you can try yourself.

## KMeans Clustering

KMeans is one of the more common and easy to understand unsupervised learning tools. It groups items (in this case toots/posts) into clusters based on their content. If for example you write about coffee, raspberry pi stuff, and death metal, those posts could naturally group themselves into clusters. They might not though! It kind of depends on how you write.

